# Process For Test Demo

This automation opens Wikipedia pages, then copies and saves some of its contents to a notepad file.

# Requirements

-   This automation was built with UiPath Studio 2021.10.5, so for some of its activities and packages to run in UiPath Studio, it may require a version that is not lower than this.
-   For the automation to operate seamlessly, save two(2) notepad files each titled 'AI Content' and 'RPA Content' respectively in the default location where new notepad files are saved.





